// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// GetUserProjectsRespBody represents a structure
type GetUserProjectsRespBody struct {
	Links    Links     `json:"links" validate:"nonzero"`
	Projects []Project `json:"projects" validate:"nonzero"`
}

// Reset set GetUserProjectsRespBody zero value
func (s *GetUserProjectsRespBody) Reset() {
	s.Links.Reset()
	s.Projects = s.Projects[:0]
}

// Validate check if the data is legal
func (s GetUserProjectsRespBody) Validate() error {
	return validator.Validate(s)
}
