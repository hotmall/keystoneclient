// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ListGroupsRespBody represents a structure
type ListGroupsRespBody struct {
	Groups []Group `json:"groups" validate:"nonzero"`
	Links  Links   `json:"links" validate:"nonzero"`
}

// Reset set ListGroupsRespBody zero value
func (s *ListGroupsRespBody) Reset() {
	s.Groups = s.Groups[:0]
	s.Links.Reset()
}

// Validate check if the data is legal
func (s ListGroupsRespBody) Validate() error {
	return validator.Validate(s)
}
