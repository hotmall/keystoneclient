// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// CreateAppCredentialRespBody represents a structure
type CreateAppCredentialRespBody struct {
	ApplicationCredential AppCredential `json:"application_credential" validate:"nonzero"`
}

// Reset set CreateAppCredentialRespBody zero value
func (s *CreateAppCredentialRespBody) Reset() {
	s.ApplicationCredential.Reset()
}

// Validate check if the data is legal
func (s CreateAppCredentialRespBody) Validate() error {
	return validator.Validate(s)
}
