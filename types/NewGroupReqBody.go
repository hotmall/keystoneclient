// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// NewGroupReqBody represents a structure
type NewGroupReqBody struct {
	Group Group `json:"group" validate:"nonzero"`
}

// Reset set NewGroupReqBody zero value
func (s *NewGroupReqBody) Reset() {
	s.Group.Reset()
}

// Validate check if the data is legal
func (s NewGroupReqBody) Validate() error {
	return validator.Validate(s)
}
