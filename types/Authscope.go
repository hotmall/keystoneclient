// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// Authscope represents a structure
type Authscope struct {
	Domain  *AuthDomain       `json:"domain,omitempty"`
	Project *Authscopeproject `json:"project,omitempty"`
	System  *Authscopesystem  `json:"system,omitempty"`
}

// Reset set Authscope zero value
func (s *Authscope) Reset() {
	if s.Domain != nil {
		s.Domain.Reset()
	}
	if s.Project != nil {
		s.Project.Reset()
	}
	if s.System != nil {
		s.System.Reset()
	}
}

// Validate check if the data is legal
func (s Authscope) Validate() error {
	return validator.Validate(s)
}
