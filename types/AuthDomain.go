// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// AuthDomain represents a structure
type AuthDomain struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

// Reset set AuthDomain zero value
func (s *AuthDomain) Reset() {
	s.ID = ""
	s.Name = ""
}

// Validate check if the data is legal
func (s AuthDomain) Validate() error {
	return validator.Validate(s)
}
