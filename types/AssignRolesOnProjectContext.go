// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// AssignRolesOnProjectContext is the context of method AssignRolesOnProject
type AssignRolesOnProjectContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	GroupID    string `json:"group_id" validate:"nonzero"`
	ProjectID  string `json:"project_id" validate:"nonzero"`
	RoleID     string `json:"role_id" validate:"nonzero"`
}

// NewAssignRolesOnProjectContext new an context instance
func NewAssignRolesOnProjectContext() *AssignRolesOnProjectContext {
	return &AssignRolesOnProjectContext{}
}

// Validate check if the data is legal
func (s AssignRolesOnProjectContext) Validate() error {
	return validator.Validate(s)
}
