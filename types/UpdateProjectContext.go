// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// UpdateProjectContext is the context of method UpdateProject
type UpdateProjectContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	ProjectID  string `json:"project_id" validate:"nonzero"`
}

// NewUpdateProjectContext new an context instance
func NewUpdateProjectContext() *UpdateProjectContext {
	return &UpdateProjectContext{}
}

// Validate check if the data is legal
func (s UpdateProjectContext) Validate() error {
	return validator.Validate(s)
}
