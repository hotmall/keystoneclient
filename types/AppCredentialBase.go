// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gitlab.com/hotmall/keystoneclient/goraml"
	"gopkg.in/validator.v2"
)

// AppCredentialBase represents a structure
type AppCredentialBase struct {
	ExpiresAt *goraml.DateTime `json:"expires_at,omitempty"`
	Name      string           `json:"name" validate:"nonzero"`
	Roles     []Role           `json:"roles,omitempty"`
}

// Reset set AppCredentialBase zero value
func (s *AppCredentialBase) Reset() {
	if s.ExpiresAt != nil {
		s.ExpiresAt.Reset()
	}
	s.Name = ""
	s.Roles = s.Roles[:0]
}

// Validate check if the data is legal
func (s AppCredentialBase) Validate() error {
	return validator.Validate(s)
}
