// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// Links represents a structure
type Links struct {
	Next     string `json:"next,omitempty"`
	Previous string `json:"previous,omitempty"`
	Self     string `json:"self,omitempty"`
}

// Reset set Links zero value
func (s *Links) Reset() {
	s.Next = ""
	s.Previous = ""
	s.Self = ""
}

// Validate check if the data is legal
func (s Links) Validate() error {
	return validator.Validate(s)
}
