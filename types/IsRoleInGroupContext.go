// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// IsRoleInGroupContext is the context of method IsRoleInGroup
type IsRoleInGroupContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	DomainID   string `json:"domain_id" validate:"nonzero"`
	GroupID    string `json:"group_id" validate:"nonzero"`
	RoleID     string `json:"role_id" validate:"nonzero"`
}

// NewIsRoleInGroupContext new an context instance
func NewIsRoleInGroupContext() *IsRoleInGroupContext {
	return &IsRoleInGroupContext{}
}

// Validate check if the data is legal
func (s IsRoleInGroupContext) Validate() error {
	return validator.Validate(s)
}
