// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// UpdateDomainContext is the context of method UpdateDomain
type UpdateDomainContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	DomainID   string `json:"domain_id" validate:"nonzero"`
}

// NewUpdateDomainContext new an context instance
func NewUpdateDomainContext() *UpdateDomainContext {
	return &UpdateDomainContext{}
}

// Validate check if the data is legal
func (s UpdateDomainContext) Validate() error {
	return validator.Validate(s)
}
