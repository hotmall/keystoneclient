#!/bin/bash
hot client -l go --kind requests --package keystoneclient --module gitlab.com/hotmall/keystoneclient
go generate
go mod tidy
go build
