module gitlab.com/hotmall/keystoneclient

go 1.21

require (
	github.com/hotmall/requests v1.1.0
	gopkg.in/validator.v2 v2.0.1
)

require (
	github.com/andybalholm/brotli v1.0.6 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.51.0 // indirect
)
