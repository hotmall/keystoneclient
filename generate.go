package keystoneclient

//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/domain.raml --import-path gitlab.com/hotmall/keystoneclient
//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/group.raml --import-path gitlab.com/hotmall/keystoneclient
//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/project.raml --import-path gitlab.com/hotmall/keystoneclient
//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/role.raml --import-path gitlab.com/hotmall/keystoneclient
//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/token.raml --import-path gitlab.com/hotmall/keystoneclient
//go:generate go-raml client --language go --kind requests --package keystoneclient --ramlfile api/user.raml --import-path gitlab.com/hotmall/keystoneclient

